(ns ch2.update
  (:require [medley.core :refer (map-keys)]))

(defn keywordize-entity
  [entity]
  (map-keys keyword entity))

(keywordize-entity {"name" "earth"
                    "moon" 1
                    "volume" 1.08321e12
                    "mass" 5.97219e24
                    "aphelion" 152098232
                    "perihelion" 147098290})