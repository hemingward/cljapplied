(ns catalog-import.core
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv]))

(defrecord CatalogItem [num dept desc price])

(defn import-catalog [data]
  (reduce #(conj %1 %2) [] data))

(defn import-catalog-fast [data]
  (persistent!
    (reduce #(conj! %1 %1) (transient []) data)))