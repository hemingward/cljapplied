(ns ch1.recipe)

(defrecord Recipe 
  [name         ;; string
   author       ;; recipe creator
   description  ;; string
   ingredients  ;; list of ingredients
   steps        ;; sequence of string
   servings     ;; number of servings
   ])

(defrecord Ingredient
  [name     ;; string
   quantity ;; amount
   unit     ;; keyword
   ])

(defrecord Person
  [fname  ;; first name
   lname  ;; last name
   ])