(ns ch1.validate
  (:require [schema.core :as s]))

(s/defrecord  Ingredient
     [name      :- s/Str
      quantity  :- s/Int
      unit      :- s/Keyword])

(s/defrecord Recipe
     [name        :- s/Str
      description :- s/Str
      ingredients :- [Ingredient]
      steps       :- [s/Str]
      servings    :- s/Int])

(s/defn add-ingredients :- Recipe
        "Adds a vector of ingredients to the recipe's ingredients."
        [recipe :- Recipe & ingredients :- [Ingredient]]
        (update-in recipe [:ingredients] into ingredients))