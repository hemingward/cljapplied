(ns ch1.apollo)

(defn euclidean-norm [ecc-vector]
  ecc-vector)

(defrecord Planet [name
                   moons
                   volume       ;; km^3                   
                   mass         ;; kg
                   aphelion     ;; km, farthest from sun
                   perihelion   ;; km, closest to sun
                   orbital-eccentricity])

(defn make-planet
  "Make a planet from field values and an eccentricity vector"
  [name moons volume mass aphelion perihelion ecc-vector]  
  (->Planet name moons volume mass aphelion perihelion (euclidean-norm ecc-vector)))

(def mission-defaults {:orbits 0
                       :evas 0})

(defn make-mission
  [name system launched manned? & opts]
  (let [{:keys [cm-name ;;command module
                lm-name ;; lunar module
                orbits
                evas]
         :or {orbits 0, evas 0}} opts]))

(def apollo-4
  (make-mission "Apollo 4"
                "Saturn V"
                #inst "1967-11-09T12:00:00-00:00"
                false
                :orbits 3))
