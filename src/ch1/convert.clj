(ns ch1.convert)

(defmulti convert
  "convert quantity from uni1 to uni2, matching on [unit1 unit2]"
  (fn [unit1 unit2 quantity] [unit1 unit2]))

;; lb to oz
(defmethod convert [:lb :oz] [_ _ lb] (* lb 16))

;; oz to lb
(defmethod convert [:oz :lb] [_ _ oz] (/ oz 16))

;; oz to g
(defmethod convert [:oz :g] [_ _ oz] (* oz 28.3495))

;; L to mL
(defmethod convert [:L :ml] [_ _ L] (* L 1000))

;; mL to L
(defmethod convert [:ml :L] [_ _ ml] (/ ml 1000))

;; fallthrough
(defmethod convert :default [u1 u2 q]
  (if (= u1 u2)
    q
    (assert false (str "Unknown unit conversion from " u1 " to " u2))))

(defn ingredient+
  "add two ingredients into a single ingredient, combining their 
  quantities with unit conversion if necessary."
  [{q1 :quantity u1 :unit :as i1}
   {q2 :quantity u2 :unit}]
  (assoc i1 :quantity (+ q1 (convert u2 u1 q2))))