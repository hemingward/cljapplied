(ns ch1.protocols)

(defprotocol Cost
  (cost [entity store]))

(defprotocol TaxedCost
  (taxed-cost [entity store]))

(extend-protocol Cost
  Recipe
  (cost [recipe store]
        (reduce +$ zero-dollars
                (map #(cost % store) (:ingredients recipe))))
  Ingredient
  (cost [ingredient store]
        (cost-of store ingredient)))

(extend-protocol TaxedCost
  Object                      ;; default fallthrough
  (taxed-cost [entity store]
              (if (satisfies? Cost entity)
                (do (extend-protocol TaxedCost
                      (class entity)
                      (taxed-cost [entity store]
                                  (* (cost entity store)
                                     (+ 1 (tax-rate store)))))
                  (taxed-cost entity store))
                (assert false (str ("Uhhandled entity: " entity))))))